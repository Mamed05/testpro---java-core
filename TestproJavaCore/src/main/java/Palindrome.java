import java.util.Scanner;

class Polindrome {

    public static void main(String[] args) {

        Scanner type = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String line = type.nextLine();
        int a, b;
        a = 0;
        b = line.length() - 1;

        while(a < b) {
            while (line.charAt(a) == ' ') {
                a++;
            }
            while (line.charAt(b) == ' ') {
                b--;
            }
            if (line.charAt(a) == line.charAt(b)) {
                a++;
                b--;
            } else {
                break;
            }
        }

        if (a >= b) {
            System.out.println(line + "This string is palindrome");
        }
        else {
            System.out.println(line + "This string is not palindrome");
        }
    }
}